## How to use
Easiest way to make requests to the API is via something like 
PostMan: https://www.getpostman.com/

Once you install the project following the outline below you will be able to access the 
API endpoint at: http://localhost:4000/v1/destination

Just make an empty POST request to the URL above and you will see the response in PostMan

## Installation
 
**You will need to ensure you have npm >3x installed globally**

````
sudo npm install npm -g
````

Run development server locally

````
git clone https://bansawbanchee@bitbucket.org/bansawbanchee/fanhero-challenge.git
cd fanhero-challenge && npm install
````

Start dev server with hot loading. (.env file is autoloaded)

````
npm start
````

## Folder Structure
* src/ - contains application code
    * services/ � contains folders pertaining to each endpoint
        * ENDPOINT_DIR/ - e.g. destinations/
            * controller.js - defines the api routes and their logic
            * model.js - represents data, interacts with database(s)
    * middlewares/ � Express middlewares which process the incoming requests before handing them down to the routes (custom middleware)
* server.js � initializes the app and glues everything together
* .env - all our environment variables are loaded on server startup(only meant for development)
