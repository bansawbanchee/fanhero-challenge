const express = require('express');
const app = express();

// parse post request body into req.body
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// include all endpoint classes on application load
app.use(require('./src/services/index'));

// custom error handler middleware
const errorHandler = require('./src/middlewares/errors');
app.use(errorHandler);

//create server and listen on port
app.listen(process.env.PORT);
console.log('API can be found here: http://localhost:'+process.env.PORT+'');