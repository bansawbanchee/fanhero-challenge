/**
 * this is a very basic error handler for requests
 * I didn't want to go to deep and customize the response format etc
 */

function errorHandler(err, req, res, next) {

    const error = {
        status: err.statusCode || 500,
        statusText: err.statusText || 'Internal Server Error',
        message: err.message || null,
        errors: [err],
        stacktrace: req.query.stacktrace ? JSON.stringify(err.stack) : null
    };

    // so you can see any errors
    console.log(error);

    if(!error.stacktrace)
        delete error.stacktrace;

    return res.status(error.status).json(error);

}

module.exports = errorHandler;