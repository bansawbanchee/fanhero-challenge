/**
 * this file instantiates all route controller classes
 * we could loop through the folders and instantiate them dynamically but
 * then you lose control over adding specific parameters for specific endpoints
 * or adding a v2 endpoint etc. We also don't want to clutter the server.js file
 */
const express = require('express');
const routes = express.Router();
const apiV1 = express.Router();

// setup /v1 base endpoint for API version control
routes.use('/v1', apiV1);

// instantiate controllers
const DestinationController = require('./destinations/controller');
new DestinationController(apiV1);

// export routes
module.exports = routes;