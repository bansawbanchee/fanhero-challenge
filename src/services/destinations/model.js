"use strict";
/**
 * normally the model would be used to integrate with the database
 * for this challenge it will be used for third-party API requests
 * we use request-promise as promises are better than callbacks ;)
 * we could make this method below more generic but I prefer to
 * define methods for each request as it makes it easier to understand
 * and modify without the need to worry about breaking other endpoints.
 *
 * We could have allowed the client to pass us Ticket/Search but I prefer to hide internal
 * endpoints and define them in the model
*/

const request = require('request-promise');

module.exports = {

    getDestination: function (auth, payload) {

        //configure options
        const options = {
            method: 'POST',
            uri: process.env.API_BASE_ENDPOINT+'/api/Ticket/Search',
            form: payload,
            json: true ,
            auth: {
                bearer: auth.access_token
            }
        };

        // do request. response is passed to controller
        return request(options);

    },

    getAccessToken: function () {

        /** configure options. for the purpose of the challenge we hardcode the authentication. review controller logic
         * for what I would normally do in production
         */
        const options = {
            method: 'POST',
            uri: process.env.API_BASE_ENDPOINT+'/Token',
            body: 'grant_type=password&username=test1%40test2.com&password=Aa234567%21'
        };

        // do request. response is passed to controller
        return request(options);

    }


};
