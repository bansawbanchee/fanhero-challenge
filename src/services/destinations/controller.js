"use strict";
/**
 * we use an ES6 class here as it makes it easier to deal with the routes for each service
 * this is the controller for the destination service. They map to the model, Let the controller
 * handle the request validation and returning of data. The model should not care how it gets data.
 * This is called loose coupling. We also require use strict on this page since node.js currently does not support
 * classes by default
 */
const destination = require('./model');

class DestinationController {

    //instantiate class
    constructor(router) {
        this.router = router;
        this._registerRoutes();
    }

    // register routes and bind methods below to routes
    _registerRoutes() {
        this.router.post('/destination/', this.requestDestination.bind(this));
    }


    requestDestination(req, res, next) {

        /** we are going to fake a request by assigning what the user would normally request to a
         * variable for the challenge. We will assign it to the request object as body
         */

        // fake it till we make it
        req.body = {
            "Language": "ENG",
            "Currency": "USD",
            "destination": "MCO",
            "DateFrom": "05/01/2017",
            "DateTO": "05/29/2017",
            "Occupancy": {
                "AdultCount": "1",
                "ChildCount": "1",
                "ChildAges": ["10"]
            }
        };

        /** lets get the access token. In this challenge we go ahead and request a new token on every request
         *  in production we would probably assign a user to the access token and use something like JWT tokens
         *  for authentication. I would probably handle this in a middleware as well
         */
        return destination.getAccessToken()
            .then(response => {
                // pass the faked req.body and the access token to the model to request the destination
                return destination.getDestination(JSON.parse(response), req.body)
            })
            .then(response => {
                // return the destination to the client
                return res.json(response);
            })
            // send errors to express error middleware
            .catch(next);
    }

}

module.exports = DestinationController;